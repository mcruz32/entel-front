import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HeaderComponent} from '../header/header.component'
import {SidebarComponent} from '../sidebar/sidebar.component'
import {UserListComponent} from './user/user-list/user-list.component'
import {RolListComponent} from './roles/rol-list/rol-list.component'
import {AuthGuard} from 'src/app/services/guard/auth.guard'

const routes: Routes = [
  {
    path: 'admin', canActivate: [AuthGuard], children: [
      { path: 'users', component: UserListComponent },
      { path: 'roles', component: RolListComponent},
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: SidebarComponent, outlet: 'sidebar' }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityRoutingModule { }
