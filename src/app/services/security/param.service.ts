import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserFormParams } from '../../entities/security/user-form-params';
import { Observable } from 'rxjs';

import {environment} from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ParamService {

  constructor(protected client: HttpClient) { }

  getParams(): Observable<UserFormParams> {

    return this.client.get<UserFormParams>(environment.baseUrl + '/params/user-params')
  }
}
