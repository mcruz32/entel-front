import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { stringify } from 'querystring';

export type SnackBarConfig = MatSnackBarConfig<any> & {msg: string, action?: string}

export class SnackbarHelper {

    static show(snackBar: MatSnackBar, config: SnackBarConfig): void {

        let defConfig: SnackBarConfig = {
            msg: '',
            duration: 3000,
            action: '',
            panelClass: ['success-snackbar'],
            horizontalPosition: 'center',
            verticalPosition: 'top',
        }

        config = { ...defConfig, ...config }

        snackBar.open(config.msg, config.action, config)


    }
}
