import {User} from 'src/app/entities/security/user'
import { Rol } from 'src/app/entities/security/rol'

export class Util {

    static onlyIds(entity: any): number[] {
        let toMap = Array.isArray(entity)? entity: []
        return toMap.map(e => e.id)
    }
}
